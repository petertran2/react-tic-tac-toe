import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'

function Square(props) {
  return (
  	<button
  		className={props.className}
  		onClick={props.onClick}>
  		{props.value}
  	</button>
  )
}

class Board extends React.Component {
  renderSquare(i) {
    return (
    	<Square
    		key={i}
    		className={
    			this.props.victory.includes(i) ?
    			'square highlight' : 'square'
    		}
    		value={this.props.squares[i]}
    		onClick={() => this.props.onClick(i)} />
    )
  }

  render() {
		let board = []
		let position = 0
		for (let i = 0; i < 3; i++) {
			let squares = []
			for (let j = 0; j < 3; j++) {
				squares.push(this.renderSquare(position))
				position += 1
			}
			board.push(
				<div
					key={(position + 1) / 3}
					className="board-row">
					{squares}
				</div>
			)
		}
    return (
      <div>{board}</div>
    )
  }
}

class Game extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			history: [{
				squares: Array(9).fill(null)
			}],
			stepNumber: 0,
			xIsNext: true,
			location: [''],
			reverseOrder: false
		}
	}
	
	handleClick(i) {
		const history = this.state.history.slice(0,
			this.state.stepNumber + 1)
		const location = this.state.location.slice(0,
			this.state.stepNumber + 1)
		const current = history[history.length - 1]
		const squares = current.squares.slice()
		if (isWinner(squares) || squares[i]) {
			return
		}
		squares[i] = this.state.xIsNext ? 'X' : 'O'
		this.setState({
			history: history.concat([{
				squares: squares
			}]),
			stepNumber: history.length,
			xIsNext: !this.state.xIsNext,
			location: location.concat(
				this.getLocation(i)
			)
		})
	}
	
	getLocation(i) {
		switch(i) {
			case 0:
				return '(1, 1)'
			case 1:
				return '(2, 1)'
			case 2:
				return '(3, 1)'
			case 3:
				return '(1, 2)'
			case 4:
				return '(2, 2)'
			case 5:
				return '(3, 2)'
			case 6:
				return '(1, 3)'
			case 7:
				return '(2, 3)'
			case 8:
				return '(3, 3)'
			default:
				return ''
		}
	}
	
	jumpTo(step) {
		this.setState({
			stepNumber: step,
			xIsNext: (step % 2) === 0
		})
	}
	
  render() {
  	const history = this.state.history
  	const current = history[this.state.stepNumber]
  	const winner = isWinner(current.squares) ? 
  		isWinner(current.squares)[0] : null
  	
  	const moves = history.map((step, move) => {
  		const desc = move ? 'Go to move #' + move :
  			'Go to game start'
  		return (
  			<li key={move}>
  				<button className={move ===
							this.state.stepNumber ? 'bold' : ''}
  					onClick={() => {
							this.jumpTo(move)
						}}>{desc} {this.state.location[move]}
  				</button>
  			</li>
  		)
  	})
  	
  	let status
  	if (winner) {
  		status = 'Winner: ' + winner
  	} else if (draw(current.squares)) {
  		status = 'Draw'
  	} else {
  		status = 'Next player: ' +
  			(this.state.xIsNext ? 'X' : 'O')
  	}
  	
    return (
      <div className="game">
        <div className="game-board">
          <Board
          	victory={isWinner(current.squares) ?
          		isWinner(current.squares)[1] : []}
          	squares={current.squares}
          	onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
        	<div><button
        		onClick={() => {
        			this.setState({reverseOrder: !this.state.reverseOrder})
        		}}>Toggle Order</button></div>
          <div>{status}</div>
          <ol>{!this.state.reverseOrder ? moves : moves.reverse()}</ol>
        </div>
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

// isWinner = calculateWinner

function isWinner(squares) {
	const lines = [
		[0, 1, 2],
		[3, 4, 5],
		[6, 7, 8],
		[0, 3, 6],
		[1, 4, 7],
		[2, 5, 8],
		[0, 4, 8],
		[2, 4, 6]
	]
	
	for (let i = 0; i < lines.length; i++) {
		const [a, b, c] = lines[i]
		if (squares[a] && squares[a] === squares[b]
			&& squares[a] === squares[c]) {
				return [squares[a], lines[i]]
			}
	}
	return null
}

function draw(squares) {
	for (let i = 0; i < squares.length; i++) {
		if (squares[i] === null) {
			return false
		}
	}
	return true
}
